Simple test project demonstrating some of the capabilities of angular and firebase.

Developed using:
 - yeoman
 - grunt
 - bower
 - angular.js
 - firebase.js
 - angularfire.js
 - angular-chart.js (http://jtblin.github.io/angular-chart.js/)

To Use:
 - Ensure node.js is installed
 - Clone a copy
 - cd into top level directory
 - npm install bower -g
 - npm install grunt -g
 - npm install grunt-cli -g
 - run 'npm install'
 - run 'bower install'
 - run 'grunt serve'

For a live, deployed demo, please see:
https://sector12consulting.firebaseapp.com/#/