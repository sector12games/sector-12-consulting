'use strict';

/* exported app */

/**
 * @ngdoc overview
 * @name sector12ConsultingApp
 * @description
 * # sector12ConsultingApp
 *
 * Main module of the application.
 */
var app = angular
  .module('sector12ConsultingApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase',
    'chart.js'
  ])
  .constant('FIREBASE_URL', 'https://Sector12Consulting.firebaseio.com/')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/clientList', {
        templateUrl: 'views/clientList.html',
        controller: 'ClientListCtrl'
      })
        .when('/clientDetails/:id', {
            templateUrl: 'views/clientDetails.html',
            controller: 'ClientDetailsCtrl'
      })
      .when('/contractList', {
        templateUrl: 'views/contractList.html',
        controller: 'ContractListCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
