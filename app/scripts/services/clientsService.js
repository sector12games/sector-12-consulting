'use strict';

/**
 * @ngdoc service
 * @name sector12ConsultingApp.clientsService
 * @description
 * # clientsService
 * Factory in the sector12ConsultingApp.
 */
app.factory('clientsService', function($firebase, FIREBASE_URL) {

    // Create a reference to the firebase location. No data is fetched
    // from the server until $asArray() or $asObject() are called.
    // Data can even be pushed using $set, $add(), $remove(), etc, 
    // without ever fetching any data to our client.
    var ref = new Firebase(FIREBASE_URL);
    var sync = $firebase(ref.child('clients'));

    // Create a synchronized object, all server changes are downloaded in realtime
    var clients = sync.$asArray();

    // Create our factory object, which angular can inject into our controllers.
    var factory = {
        // Return all clients
        all: clients,

        // Get a single client, by unique id (firebase push id).
        get: function(clientId) {
            // While you can technically call clients.$getRecord(id), it's
            // not safe, because it's possible to call this method before
            // clients is fully downloaded. Use the firebase ref instead.
            var clientRef = sync.$ref().child(clientId);
            var client = $firebase(clientRef).$asObject();
            return client;
        },

        // Add a client to firebase
        add: function(client) {
            return clients.$add(client);
        },

        // Remove a client from firebase
        remove: function (client)
        {
            return clients.$remove(client);
        },

        // Return all contracts for the given client
        getClientContracts: function (client) {
            var contractsRef = sync.$ref().child(client.$id).child('contracts');
            var contracts = $firebase(contractsRef).$asArray();
            return contracts;
        }
    };

    // Watch all clients, and recalculate some aggregate data whenever
    // they change.
    clients.$watch(function (event) {
        // No need to recalculate for child_moved or child_removed
        if (event.event === 'child_added' || event.event === 'child_changed') {
            // Find the client by it's id
            var client = findClient(event.key);

            // Add a fullName field for easy client side filtering
            client.fullName = client.firstName + ' ' + client.lastName;

            // Calculate some aggregate data specific to this client, and attach
            // it to the client object for future use. This does not cause the 
            // $watch function to fire again.
            client.contractTotals = getClientContractTotals(client);
        }
    });

    // Finds a single client (that's already on the client side) given the
    // client's id
    function findClient(clientId) {
        for (var i = 0; i < clients.length; i++) {
            if (clients[i].$id === clientId) {
                return clients[i];
            }
        }
    }

    // Iterate over all of the client's contracts. Remember that client.contracts is 
    // not an array, it is a list of objects.
    // http://stackoverflow.com/questions/684672/loop-through-javascript-object
    function getClientContractTotals(client) {
        var contractTotals = { totalContracts: 0, totalBillableHours: 0, totalRevenue: 0 };

        // Iterate over our object list differently than you would a standard array
        for (var contractKey in client.contracts) {
            // Make sure we're only iterating over our own, direct objects, and
            // not objects in the prototype chain.
            if (client.contracts.hasOwnProperty(contractKey)) {
                var contract = client.contracts[contractKey];

                contractTotals.totalContracts++;

                var hours = parseInt(contract.hours);
                if (!isNaN(hours)) {
                    contractTotals.totalBillableHours += hours;
                }

                var cost = parseInt(contract.cost);
                if (!isNaN(cost)) {
                    contractTotals.totalRevenue += cost;
                }
            }
        }

        return contractTotals;
    }

    return factory;
});


