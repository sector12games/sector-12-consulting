﻿'use strict';

/**
 * @ngdoc function
 * @name sector12ConsultingApp.controller:ClientDetailsCtrl
 * @description
 * # ClientDetailsCtrl
 * Controller of the sector12ConsultingApp
 */
app.controller('ClientDetailsCtrl', function ($scope, $routeParams, clientsService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.client = clientsService.get($routeParams.id);
    $scope.contracts = clientsService.getClientContracts($scope.client);
    $scope.contract = { task: '', hours: '', cost: '' };

    // Add the contract to firebase
    $scope.addContract = function () {
        $scope.contracts.$add($scope.contract).then(function() {
            $scope.contract = { task: '', hours: '', cost: '' };
        });
    };

    // Remove the contract from firebase
    $scope.removeContract = function (contract) {
        $scope.contracts.$remove(contract);
    };
});
