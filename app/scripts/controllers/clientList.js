'use strict';

/**
 * @ngdoc function
 * @name sector12ConsultingApp.controller:ClientListCtrl
 * @description
 * # ClientListCtrl
 * Controller of the sector12ConsultingApp
 */
app.controller('ClientListCtrl', function ($scope, clientsService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.client = { firstName: '', lastName: '', country: '' };
    $scope.clients = clientsService.all;

    // Add a client to firebase
    $scope.addClient = function () {
        clientsService.add($scope.client).then(function () {
            $scope.client = { firstName: '', lastName: '', country: '' };
        });
    };

    // Remove a client from firebase
    $scope.removeClient = function (client) {
        clientsService.remove(client);
    };
});
