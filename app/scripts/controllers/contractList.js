'use strict';

/**
 * @ngdoc function
 * @name sector12ConsultingApp.controller:ContractListCtrl
 * @description
 * # ContractListCtrl
 * Controller of the sector12ConsultingApp
 */
app.controller('ContractListCtrl', function ($scope, clientsService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.clients = clientsService.all;
    $scope.searchFilter = '';

    // Counts client contracts for a single client. This is necessary because
    // angular expressions {{ }} are run against the scope, not the window,
    // so functions like Object.keys() are not available.
    $scope.countClientContracts = function (client) {
        if (client.contracts === undefined) {
            return 0;
        } else {
            return Object.keys(client.contracts).length;
        }
    };
});
