'use strict';

/**
 * @ngdoc function
 * @name sector12ConsultingApp.controller:NavbarCtrl
 * @description
 * # NavbarCtrl
 * Controller of the sector12ConsultingApp
 */
app.controller('NavbarCtrl', function ($scope, $location) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    // Used for determining whether or not the navigation item is marked as
    // 'active' (for css styling).
    // http://stackoverflow.com/questions/16199418/how-do-i-implement-the-bootstrap-navbar-active-class-with-angular-js
    $scope.isActive = function (viewLocation) {
        if (viewLocation === '/') {
            return viewLocation === $location.path();
        }
        else {
            return $location.path().indexOf(viewLocation) === 0;
        }
    };

});
