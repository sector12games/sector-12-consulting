'use strict';

/**
 * @ngdoc function
 * @name sector12ConsultingApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sector12ConsultingApp
 */
app.controller('MainCtrl', function ($scope, clientsService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var dataLoaded = false;
    $scope.clients = clientsService.all;
    $scope.totalContracts = 0;
    $scope.totalBillableHours = 0;
    $scope.totalRevenue = 0;
    $scope.countries = {};

    function calculateTotals() {
        // Reset our totals
        $scope.totalContracts = 0;
        $scope.totalBillableHours = 0;
        $scope.totalRevenue = 0;
        for (var countryName in $scope.countries) {
            $scope.countries[countryName] = 0;
        }

        // Recalculate aggregate data
        for (var i = 0; i < $scope.clients.length; i++) {
            var contractTotals = $scope.clients[i].contractTotals;
            $scope.totalContracts += contractTotals.totalContracts;
            $scope.totalBillableHours += contractTotals.totalBillableHours;
            $scope.totalRevenue += contractTotals.totalRevenue;

            // Add the number of contracts to our countries list, so that
            // we can display the total number of contracts by country. If the
            // client's country does not already exist in the list, add it.
            var client = $scope.clients[i];
            var country = client.country;
            if ($scope.countries[country] === undefined) {
                $scope.countries[country] = 0;
            }
            $scope.countries[country] += contractTotals.totalContracts;
        }
    }

    // Update the data for our various charts. The charts themselves will animate
    // and update in real time.
    function updateCharts() {
        updatePieChart();
        updateBarChart();
        //updateBarChartMulti();
        updateDoughnutChart();
    }

    // Updates our pie chart with client data. Called every time the firebase server
    // data changes.
    function updatePieChart() {
        // Clear all data
        $scope.labelsPieChart = [];
        $scope.dataPieChart = [];

        // Add data for each client
        for (var i = 0; i < $scope.clients.length; i++) {
            var client = $scope.clients[i];
            var contractTotals = client.contractTotals;
            var name = client.firstName + ' ' + client.lastName;

            $scope.labelsPieChart.push(name);
            $scope.dataPieChart.push(contractTotals.totalRevenue);
        }
    }

    // Updates our bar chart with client data. Called every time the firebase server
    // data changes.
    function updateBarChart() { // jshint ignore:line
        // Clear all data
        $scope.labelsBarChart = [];
        $scope.seriesBarChart = [];
        $scope.dataBarChart = [[]];

        // Add data for each client
        for (var i = 0; i < $scope.clients.length; i++) {
            var client = $scope.clients[i];
            var contractTotals = client.contractTotals;
            var name = client.firstName + ' ' + client.lastName;
            
            // In this bar chart, each client will have it's own label. There will
            // only be one "series" (client) per label. This is a little different than
            // the way bar charts are usually handled. Usually, they will have several
            // "series" (clients) per label. Label will typically be something like
            // "month" (jan, feb, march, etc).
            $scope.labelsBarChart.push(name);

            // Add average rate data for this client. Watch for divide by 0 situations, 
            // so that we don't assign NaN to the chart data. If the chart has any 
            // NaN values, the entire chart will break.
            var averageRate = contractTotals.totalRevenue / contractTotals.totalBillableHours;
            if (isNaN(averageRate)) {
                averageRate = 0;
            }
            $scope.dataBarChart[0].push(averageRate);
        }
    }

    // Updates our bar chart in a slightly different way. This should not be used for 
    // production, but is useful for testing and understanding the bar chart. You should 
    // only call this method OR updateBarChart() -- not both.
    function updateBarChartMulti() { // jshint ignore:line
        // Clear all data
        $scope.labelsBarChart = ['Average Rate'];
        $scope.seriesBarChart = [];
        $scope.dataBarChart = [];

        // Add data for each client
        for (var i = 0; i < $scope.clients.length; i++) {
            var client = $scope.clients[i];
            var contractTotals = client.contractTotals;
            var name = client.firstName + ' ' + client.lastName;

            // In this bar chart, there is only one label ("Average Rate"). Each client
            // will be a separte "series". 
            $scope.seriesBarChart.push(name);

            // Add average rate data for this client. Watch for divide by 0 situations, 
            // so that we don't assign NaN to the chart data. If the chart has any 
            // NaN values, the entire chart will break.
            var averageRate = contractTotals.totalRevenue / contractTotals.totalBillableHours;
            if (isNaN(averageRate)) {
                averageRate = 0;
            }
            $scope.dataBarChart.push([averageRate]);
        }
    }

    // Updates our doughnut chart with client data. Called every time the firebase server
    // data changes.
    function updateDoughnutChart() {
        // Clear all data
        $scope.labelsDoughnutChart = [];
        $scope.dataDoughnutChart = [];

        // Our doughnut chart's data is grouped by country. 
        // There's no particular reason for me to break this down into 
        // fieldname / value here, I just wanted to do it to make it 
        // clear what's happening. Remember that our countries list is
        // an object, and not an array.
        for (var countryName in $scope.countries) {
            var fieldName = countryName;
            var value = $scope.countries[countryName];

            $scope.labelsDoughnutChart.push(fieldName);
            $scope.dataDoughnutChart.push(value);
        }
    }

    // Initialize the various charts once data is loaded
    // Optional parameter: 'dataSnapshot'
    $scope.clients.$loaded().then(function () {
        calculateTotals();
        updateCharts();
        dataLoaded = true;
    });

    // Watch every element in the array for changes
    // https://www.firebase.com/docs/web/libraries/angular/api.html#angularfire-firebasearray-watchcb-context
    // Optional parameter: 'event'
    $scope.clients.$watch(function () {
        // Don't start updating the charts until all of the initial data
        // has been loaded. After the initial load, update the charts any
        // time we receive any new data from the server.
        if (dataLoaded) {
            calculateTotals();
            updateCharts();
        }
    });

});
