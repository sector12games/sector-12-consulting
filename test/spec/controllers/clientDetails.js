﻿'use strict';

describe('Controller: ClientDetailsCtrl', function () {

    // load the controller's module
    beforeEach(module('sector12ConsultingApp'));

    var ClientDetailsCtrl,
      scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        ClientDetailsCtrl = $controller('ClientDetailsCtrl', {
            $scope: scope,
            $routeParams: { id: '0' }
        });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
        expect(scope.awesomeThings.length).toBe(3);
    });
});
