﻿'use strict';

describe('Controller: ClientListCtrl', function () {

    // load the controller's module
    beforeEach(module('sector12ConsultingApp'));

    var ClientListCtrl,
      scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        ClientListCtrl = $controller('ClientListCtrl', {
            $scope: scope,
        });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
        expect(scope.awesomeThings.length).toBe(3);
    });
});
