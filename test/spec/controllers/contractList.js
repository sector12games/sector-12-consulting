﻿'use strict';

describe('Controller: ContractListCtrl', function () {

    // load the controller's module
    beforeEach(module('sector12ConsultingApp'));

    var ContractListCtrl,
      scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        ContractListCtrl = $controller('ContractListCtrl', {
            $scope: scope,
        });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
        expect(scope.awesomeThings.length).toBe(3);
    });
});
