'use strict';

describe('Service: clientsService', function () {

  // load the service's module
  beforeEach(module('sector12ConsultingApp'));

  // instantiate service
  var clientsService;
  beforeEach(inject(function (_clientsService_) {
    clientsService = _clientsService_;
  }));

  it('should do something', function () {
    expect(!!clientsService).toBe(true);
  });

});
